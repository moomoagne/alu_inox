<?php

namespace ALU\BackOfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


class BackOfficeController extends Controller
{
    /**
     * @Route("/admin", name="admin_index")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $chantiers = $em->getRepository('ALUMainBundle:Chantier')->findAll();
        $depenses = $em->getRepository('ALUMainBundle:Depense')->findAll();
        $accomptes = $em->getRepository('ALUMainBundle:Accompte')->findAll();
        $services = $em->getRepository('ALUMainBundle:Service')->findAll();

        return $this->render('ALUBackOfficeBundle:Default:index.html.twig',[
            'chantiers' =>$chantiers,
            'depenses' =>$depenses,
            'accomptes' =>$accomptes,
            'services' =>$services,
        ]);
    }
}
