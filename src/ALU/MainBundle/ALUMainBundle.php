<?php

namespace ALU\MainBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ALUMainBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
