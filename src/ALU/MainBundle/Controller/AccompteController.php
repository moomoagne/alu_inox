<?php

namespace ALU\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class AccompteController extends Controller
{
    /**
     * @Route("/admin/accomptes", name="accompte_index")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $accomptes = $em->getRepository('ALUMainBundle:Accompte')
            ->findAll();

        return $this->render('ALUMainBundle:Accompte:index.html.twig', array(
            'accomptes' => $accomptes,

        ));
    }

}
