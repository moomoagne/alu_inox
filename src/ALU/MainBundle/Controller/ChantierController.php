<?php

namespace ALU\MainBundle\Controller;

use ALU\MainBundle\Entity\Accompte;
use ALU\MainBundle\Entity\Chantier;
use ALU\MainBundle\Entity\Depense;
use ALU\MainBundle\Event\AccompteEvent;
use ALU\MainBundle\Event\ChantierEvent;
use ALU\MainBundle\Event\DepenseEvent;
use ALU\MainBundle\Event\NotificationEvent;
use ALU\MainBundle\Form\AccompteType;
use ALU\MainBundle\Form\ChantierType;
use ALU\MainBundle\Form\DepenseType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ChantierController
 * @package ALU\MainBundle\Controller
 * @Route("/admin")
 */
class ChantierController extends Controller
{
    /**
     * @Route("/chantiers", name="chantier_index")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $chantiers = $em->getRepository('ALUMainBundle:Chantier')
            ->findBy(array(), ['dateDebut' => 'desc'], 3, 0)
        ;
        return $this->render('ALUMainBundle:Chantier:index.html.twig', array(
            'chantiers' => $chantiers
        ));
    }

    /**
     * @Route("/chantiers/all", name="chantier_all")
     */
    public function allChantierAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $listschantiers = $em->getRepository('ALUMainBundle:Chantier')
            ->findAll();
        $chantiers = $this->get('knp_paginator')->paginate(
            $listschantiers,
            $request->query->get('page', 1),
            6
        );
        return $this->render('ALUMainBundle:Chantier:all.html.twig',array(
            'chantiers' => $chantiers,
            'listschantiers' => $listschantiers,
        ));
    }

    /**
     * @Route("/chantiers/new" , name="chantier_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $chantiers = new Chantier();
        $form = $this->createForm(ChantierType::class, $chantiers);
        $form->handleRequest($request);

        $user = $this->container->get('security.token_storage')->getToken()->getUser();

        if ($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();

            $chantiers->setUser($user);
            $em->persist($chantiers);
            $em->flush();

            $request->getSession()
                ->getFlashBag()
                ->add('notice', 'Chantier ' .$chantiers->getName(). ' a bien été creer !');
            $this->dispatch(NotificationEvent::NEW_CHANTIER, new ChantierEvent($chantiers));
            return $this->redirectToRoute('chantier_show', ['id' => $chantiers->getId()]);
        }
        return $this->render('ALUMainBundle:Chantier:new.html.twig',[
            'chantier' => $chantiers,
            'form'     => $form->createView(),
        ]);
    }

    /**
     * @param Request $request
     * @param Chantier $chantier
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     *
     *@Route("chantiers/{id}/accompte", name="add_accompte_to_chantier")
     * @Method("POST")
     */
    public function newAcompteAction(Request $request, Chantier $chantier)
    {
        $accompte = new Accompte();
        $form = $this->createForm(AccompteType::class, $accompte);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $accompte->setChantier($chantier);
            $em->persist($accompte);
            $em->flush();
            $this->addFlash('notice', 'Accompte créer');
            $this->dispatch(NotificationEvent::NEW_ACCOMPTE, new AccompteEvent($accompte));
            return $this->redirectToRoute('chantier_show',['id' =>$chantier->getId()]);

        }
        else
        {
            $this->addFlash('error','Erreur');
        }
        return $this->render('ALUMainBundle:Chantier:show.html.twig',[
            'accompte' => $accompte,
            'form' => $form->createView()
        ]);
    }

    /**
     * @param Request $request
     * @param Chantier $chantier
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Route("chantiers/{id}/depense", name="add_depense_to_chantier")
     */
    public function newDepenseAction(Request $request, Chantier $chantier)
    {
        $depense = new Depense();
        $form = $this->createForm(DepenseType::class, $depense);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $depense->setChantier($chantier);
            $em->persist($depense);
            $em->flush();
            $this->addFlash('notice', 'Depense créer');
            $this->dispatch(NotificationEvent::NEW_DEPENSE, new DepenseEvent($depense));
            return $this->redirectToRoute('chantier_show',['id' =>$chantier->getId()]);

        }
        else
        {
            $this->addFlash('error','Erreur');
        }
        return $this->render('ALUMainBundle:Chantier:show.html.twig',[
            'depense' => $depense,
            'form' => $form->createView()
        ]);
    }

    /**
     * Finds and displays a chantier entity.
     *
     * @Route("/chantiers/{id}", name="chantier_show")
     * @Method("GET")
     */
    public function showAction(Chantier $chantier)
    {

        $deleteForm = $this->createDeleteForm($chantier);
        $em = $this->getDoctrine()->getManager();

        $totalDepenseOnBudget = $em->getRepository('ALUMainBundle:Depense')->getTotalDepenseForChantier($chantier);
        $totalAcoompte = $em->getRepository('ALUMainBundle:Accompte')->getTotalAccompteForChantier($chantier);


        $accompte_chantier = $em->getRepository('ALUMainBundle:Accompte')->getAccompteForChantier($chantier);
        $depense_chantier = $em->getRepository('ALUMainBundle:Depense')->getDepenseForChantier($chantier);
        //dump($accompte_chantier);
        //die();
        foreach ($totalDepenseOnBudget as $db){
            $budget_restant = $chantier->getBudget() - $db['montantTotal'];
        }
        //dump($budget_restant);die();
        if ($budget_restant < 0){
            $this->dispatch(NotificationEvent::DESACTIVER_CHANTIER, new ChantierEvent($chantier));
        }

        $accompte_form = $this->createForm(AccompteType::class, new Accompte(),array(
            'action' => $this->generateUrl('add_accompte_to_chantier',['id'=>$chantier->getId()])));

        $depense_form = $this->createForm(DepenseType::class, new Depense(),array(
            'action' => $this->generateUrl('add_depense_to_chantier',['id'=>$chantier->getId()])));


        return $this->render('ALUMainBundle:Chantier:show.html.twig', array(
            'id' => $chantier->getId(),
            'chantier' => $chantier,
            'accompte_chantier' => $accompte_chantier,
            'depense_chantier' => $depense_chantier,
            'accompte_form'=>$accompte_form->createView(),
            'depense_form'=>$depense_form->createView(),
            'delete_form' => $deleteForm->createView(),
            'totalDepenseOnBudget' => $totalDepenseOnBudget,
            'totalAccomptes' =>$totalAcoompte,
        ));
    }



    /**
     * @param Request $request
     * @param Chantier $chantier
     * @Route("/chantiers/{id}", name="chantier_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Chantier $chantier)
    {
        $form = $this->createDeleteForm($chantier);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->remove($chantier);
            $em->flush();
        }
    }

    /**
     * @Route("/chantiers/{id}/edit", name="chantier_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Chantier $chantier)
    {
        $deleteForm = $this->createDeleteForm($chantier);

        $editForm = $this->createForm('ALU\MainBundle\Form\ChantierType', $chantier);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('chantier_edit', array('id' => $chantier->getId()));
        }

        return $this->render('ALUMainBundle:Chantier:edit.html.twig', array(
            'id' => $chantier->getId(),
            'name' => $chantier->getName(),
            'chantier' => $chantier,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }


    /**
     * Creates a form to delete a chantier entity.
     *
     * @param Chantier $chantier The chantier entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Chantier $chantier)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('chantier_delete', array('id' => $chantier->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }

    private function dispatch($eventName, Event $event)
    {
        return $this->get('event_dispatcher')->dispatch($eventName, $event);
    }
}
