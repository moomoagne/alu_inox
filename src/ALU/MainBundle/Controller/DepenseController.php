<?php

namespace ALU\MainBundle\Controller;

use ALU\MainBundle\Entity\Depense;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Depense controller.
 *
 * @Route("admin/depense")
 */
class DepenseController extends Controller
{
    /**
     * Lists all depense entities.
     *
     * @Route("/", name="depense_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $depenses = $em->getRepository('ALUMainBundle:Depense')->findAll();

        return $this->render('ALUMainBundle:depense:index.html.twig', array(
            'depenses' => $depenses,
        ));
    }

    /**
     * Creates a new depense entity.
     *
     * @Route("/new", name="depense_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $depense = new Depense();
        $form = $this->createForm('ALU\MainBundle\Form\DepenseType', $depense);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($depense);
            $em->flush();

            return $this->redirectToRoute('depense_show', array('id' => $depense->getId()));
        }

        return $this->render('depense/new.html.twig', array(
            'depense' => $depense,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a depense entity.
     *
     * @Route("/{id}", name="depense_show")
     * @Method("GET")
     */
    public function showAction(Depense $depense)
    {
        $deleteForm = $this->createDeleteForm($depense);

        return $this->render('depense/show.html.twig', array(
            'depense' => $depense,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing depense entity.
     *
     * @Route("/{id}/edit", name="depense_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Depense $depense)
    {
        $deleteForm = $this->createDeleteForm($depense);
        $editForm = $this->createForm('ALU\MainBundle\Form\DepenseType', $depense);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('depense_edit', array('id' => $depense->getId()));
        }

        return $this->render('depense/edit.html.twig', array(
            'depense' => $depense,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a depense entity.
     *
     * @Route("/{id}", name="depense_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Depense $depense)
    {
        $form = $this->createDeleteForm($depense);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($depense);
            $em->flush();
        }

        return $this->redirectToRoute('depense_index');
    }

    /**
     * Creates a form to delete a depense entity.
     *
     * @param Depense $depense The depense entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Depense $depense)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('depense_delete', array('id' => $depense->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
