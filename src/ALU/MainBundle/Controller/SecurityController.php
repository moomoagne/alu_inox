<?php
/**
 * Created by PhpStorm.
 * User: agne
 * Date: 10/11/18
 * Time: 11:47 PM
 */

namespace ALU\MainBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SecurityController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function accueilAction()
    {
        return $this->render('ALUMainBundle:Default:index.html.twig');
    }
    /**
     * Renders the login template with the given parameters. Overwrite this function in
     * an extended controller to provide additional data for the login template.
     *
     * @param array $data
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function renderLogin(array $data)
    {
        return $this->container->get('templating')->renderResponse('ALUBackOfficeBundle:Security:login.html.twig', $data);
    }
}