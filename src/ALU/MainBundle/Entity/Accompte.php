<?php

namespace ALU\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Accompte
 *
 * @ORM\Table(name="accompte")
 * @ORM\Entity(repositoryClass="ALU\MainBundle\Repository\AccompteRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Accompte
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nature", type="string", length=255)
     */
    private $nature;

    /**
     * @var string
     *
     * @ORM\Column(name="montant", type="decimal", precision=16, scale=2)
     */
    private $montant;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateAccompte", type="datetime")
     */
    private $dateAccompte;

    /**
     * @var Chantier
     * @ORM\ManyToOne(targetEntity="ALU\MainBundle\Entity\Chantier", inversedBy="accompte", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    protected $chantier;

    /**
     * Accompte constructor.
     */
    public function __construct()
    {
        $this->dateAccompte =  new \DateTime('now');
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nature
     *
     * @param string $nature
     *
     * @return Accompte
     */
    public function setNature($nature)
    {
        $this->nature = $nature;

        return $this;
    }

    /**
     * Get nature
     *
     * @return string
     */
    public function getNature()
    {
        return $this->nature;
    }

    /**
     * Set montant
     *
     * @param string $montant
     *
     * @return Accompte
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;

        return $this;
    }

    /**
     * Get montant
     *
     * @return string
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * Set dateAccompte
     *
     * @param \DateTime $dateAccompte
     *
     * @return Accompte
     */
    public function setDateAccompte($dateAccompte)
    {
        $this->dateAccompte = $dateAccompte;

        return $this;
    }

    /**
     * Get dateAccompte
     *
     * @return \DateTime
     */
    public function getDateAccompte()
    {
        return $this->dateAccompte;
    }

    /**
     * @return Chantier
     */
    public function getChantier()
    {
        return $this->chantier;
    }

    /**
     * @param Chantier $chantier
     */
    public function setChantier($chantier)
    {
        $this->chantier = $chantier;
    }



}

