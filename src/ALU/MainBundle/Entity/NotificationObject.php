<?php
/**
 * Created by PhpStorm.
 * User: agne
 * Date: 10/17/18
 * Time: 3:57 PM
 */

namespace ALU\MainBundle\Entity;


class NotificationObject
{
    private $type;
    private $toEmail;
    private $mailBody;
    private $params;
    private $subject;
    private $message;

    /**
     * NotificationObject constructor.
     * @param $type
     * @param $toEmail
     * @param $mailBody
     * @param $params
     * @param $subject
     * @param $message
     */
    public function __construct($type, $toEmail, $mailBody, $params, $subject)
    {
        $this->type = $type;
        $this->toEmail = $toEmail;
        $this->mailBody = $mailBody;
        $this->params = $params;
        $this->subject = $subject;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getToEmail()
    {
        return $this->toEmail;
    }

    /**
     * @param mixed $toEmail
     */
    public function setToEmail($toEmail)
    {
        $this->toEmail = $toEmail;
    }

    /**
     * @return mixed
     */
    public function getMailBody()
    {
        return $this->mailBody;
    }

    /**
     * @param mixed $mailBody
     */
    public function setMailBody($mailBody)
    {
        $this->mailBody = $mailBody;
    }

    /**
     * @return mixed
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param mixed $params
     */
    public function setParams($params)
    {
        $this->params = $params;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param mixed $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }




}