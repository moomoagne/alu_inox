<?php

namespace ALU\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Service
 *
 * @ORM\Table(name="service")
 * @ORM\Entity(repositoryClass="ALU\MainBundle\Repository\ServiceRepository")
 */
class Service
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="natureCeintrage", type="string", length=255)
     */
    private $natureCeintrage;

    /**
     * @var string
     *
     * @ORM\Column(name="client", type="string", length=255)
     */
    private $client;

    /**
     * @var int
     *
     * @ORM\Column(name="telephone", type="integer")
     */
    private $telephone;

    /**
     * @var string
     *
     * @ORM\Column(name="montant", type="decimal", precision=16, scale=2)
     */
    private $montant;

    /**
     * @var string
     *
     * @ORM\Column(name="montantAvance", type="decimal", precision=16, scale=2)
     */
    private $montantAvance;

    /**
     * @var string
     *
     * @ORM\Column(name="montantRestant", type="decimal", precision=16, scale=2)
     */
    private $montantRestant;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createAt", type="datetime")
     */
    private $createAt;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * Service constructor.
     */
    public function __construct()
    {
        $this->createAt = new \DateTime();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set natureCeintrage
     *
     * @param string $natureCeintrage
     *
     * @return Service
     */
    public function setNatureCeintrage($natureCeintrage)
    {
        $this->natureCeintrage = $natureCeintrage;

        return $this;
    }

    /**
     * Get natureCeintrage
     *
     * @return string
     */
    public function getNatureCeintrage()
    {
        return $this->natureCeintrage;
    }

    /**
     * Set client
     *
     * @param string $client
     *
     * @return Service
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return string
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set telephone
     *
     * @param integer $telephone
     *
     * @return Service
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return int
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set montant
     *
     * @param string $montant
     *
     * @return Service
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;

        return $this;
    }

    /**
     * Get montant
     *
     * @return string
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * Set montantAvance
     *
     * @param string $montantAvance
     *
     * @return Service
     */
    public function setMontantAvance($montantAvance)
    {
        $this->montantAvance = $montantAvance;

        return $this;
    }

    /**
     * Get montantAvance
     *
     * @return string
     */
    public function getMontantAvance()
    {
        return $this->montantAvance;
    }

    /**
     * Set montantRestant
     *
     * @param string $montantRestant
     *
     * @return Service
     */
    public function setMontantRestant($montantRestant)
    {
        $this->montantRestant = $montantRestant;

        return $this;
    }

    /**
     * Get montantRestant
     *
     * @return string
     */
    public function getMontantRestant()
    {
        return $this->montantRestant;
    }

    /**
     * Set createAt
     *
     * @param \DateTime $createAt
     *
     * @return Service
     */
    public function setCreateAt($createAt)
    {
        $this->createAt = $createAt;

        return $this;
    }

    /**
     * Get createAt
     *
     * @return \DateTime
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Service
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }


}

