<?php

namespace ALU\MainBundle\Event;

use ALU\MainBundle\Entity\Accompte;
use Symfony\Component\EventDispatcher\Event;

class AccompteEvent extends Event
{
    protected $accompte;

    /**
     * AccompteEvent constructor.
     */
    public function __construct(Accompte $accompte)
    {
        $this->accompte = $accompte;
    }

    /**
     * @return Accompte
     */
    public function getAccompte()
    {
        return $this->accompte;
    }
}