<?php
/**
 * Created by PhpStorm.
 * User: agne
 * Date: 10/17/18
 * Time: 3:17 PM
 */

namespace ALU\MainBundle\Event;


use ALU\MainBundle\Entity\Chantier;
use Symfony\Component\EventDispatcher\Event;

class ChantierEvent extends Event
{
    protected $chantier;


    public function __construct(Chantier $chantier)
    {
        $this->chantier = $chantier;
    }

    /**
     * @return Chantier
     */
    public function getChantier()
    {
        return $this->chantier;
    }


}