<?php
/**
 * Created by PhpStorm.
 * User: agne
 * Date: 10/17/18
 * Time: 6:06 PM
 */

namespace ALU\MainBundle\Event;


use ALU\MainBundle\Entity\Depense;
use Symfony\Component\EventDispatcher\Event;

class DepenseEvent extends Event
{
    /**
     * @var Depense
     */
    protected $depense;

    /**
     * DepenseEvent constructor.
     * @param Depense $depense
     */
    public function __construct(Depense $depense)
    {
        $this->depense = $depense;
    }


    /**
     * @return Depense
     */
    public function getDepense()
    {
        return $this->depense;
    }



}