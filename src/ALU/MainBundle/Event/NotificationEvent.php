<?php
/**
 * Created by PhpStorm.
 * User: agne
 * Date: 10/14/18
 * Time: 10:51 PM
 */

namespace ALU\MainBundle\Event;


class NotificationEvent
{
    const NEW_ACCOMPTE = 'alu.accompte.new';
    const NEW_CHANTIER = 'alu.chantier.new';
    const NEW_DEPENSE = 'alu.depense.new';
    const ACTIVE_CHANTIER = 'alu.chantier.active';
    const DESACTIVER_CHANTIER = 'alu.chantier.active';
}