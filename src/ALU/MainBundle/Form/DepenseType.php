<?php

namespace ALU\MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DepenseType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nature')
            //->add('createAt')
            ->add('montant')
            ->add('beneficiare')
            ->add('description')
            ->add('typeDepense', ChoiceType::class,[
                'label' => 'Type de depense',
                'choices' => array('Font propre' => 'Font propre' , 'budget'=> 'budget'),
                'expanded' => true,
                'multiple' => false,
                'required' => true
            ]);
            //->add('chantier');
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ALU\MainBundle\Entity\Depense'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'alu_mainbundle_depense';
    }


}
