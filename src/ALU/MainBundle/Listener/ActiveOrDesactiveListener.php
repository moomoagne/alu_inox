<?php
/**
 * Created by PhpStorm.
 * User: agne
 * Date: 10/19/18
 * Time: 2:49 PM
 */

namespace ALU\MainBundle\Listener;


use ALU\MainBundle\Event\ChantierEvent;
use ALU\MainBundle\Event\NotificationEvent;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ActiveOrDesactiveListener implements EventSubscriberInterface
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }


    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2')))
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        // TODO: Implement getSubscribedEvents() method.
        return [
            NotificationEvent::ACTIVE_CHANTIER => 'onActiveChantier',
            NotificationEvent::DESACTIVER_CHANTIER => 'onDesactiveChantier',
        ];
    }

    public function onDesactiveChantier(ChantierEvent $event)
    {
        $chantier = $event->getChantier();
        $chantier->setIsActive(false);
        $this->em->persist($chantier);
        $this->em->flush();
    }

    public function onActiveChantier(ChantierEvent $event)
    {
        $chantier = $event->getChantier();
        $chantier->setIsActive(true);
        $this->em->persist($chantier);
        $this->em->flush();
    }

}