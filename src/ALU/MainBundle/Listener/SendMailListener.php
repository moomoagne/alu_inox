<?php
/**
 * Created by PhpStorm.
 * User: agne
 * Date: 10/17/18
 * Time: 4:09 PM
 */

namespace ALU\MainBundle\Listener;


use ALU\MainBundle\Entity\NotificationObject;
use ALU\MainBundle\Event\AccompteEvent;
use ALU\MainBundle\Event\ChantierEvent;
use ALU\MainBundle\Event\DepenseEvent;
use ALU\MainBundle\Event\NotificationEvent;
use ALU\MainBundle\Notification\AbstractNotification;
use ALU\MainBundle\Notification\NotificationManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class SendMailListener implements EventSubscriberInterface
{
    private $em;
    private $notifier;

    /**
     * SendMailListener constructor.
     * @param EntityManagerInterface $manager
     * @param AbstractNotification $notification
     */
    public function __construct(AbstractNotification $notification, EntityManagerInterface $manager)
    {
        $this->em = $manager;
        $this->notifier = $notification;
    }


    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2')))
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        // TODO: Implement getSubscribedEvents() method.
        return [
          NotificationEvent::NEW_CHANTIER => "onNewChantier",
          NotificationEvent::NEW_DEPENSE => "onNewDepense",
          NotificationEvent::NEW_ACCOMPTE => "onNewAccompte",
        ];
    }

    public function onNewChantier(ChantierEvent $event){
        $chantier = $event->getChantier();
        $user = $chantier->getUser();

        $subject = 'Création de chantier '.$chantier->getName();
        $template = 'Emails/new_chantier_email.html.twig';
        $params = ['user' => $user, 'chantier' =>$chantier];

        $notification = new NotificationObject(NotificationManager::EMAIL_ONLY, $user->getEmail(), $template, $params,$subject);
        $this->notifier->notify($notification);
    }

    public function onNewDepense(DepenseEvent $event){
        $depense = $event->getDepense();
        $user = $depense->getChantier()->getUser();
        $subject = 'Création de depense pour le chantier '.$depense->getChantier()->getName();
        $template = 'Emails/new_depense_email.html.twig';
        $params = ['user' => $user, 'depense' => $depense, 'chantier' => $depense->getChantier()];

        $notification = new NotificationObject(NotificationManager::EMAIL_ONLY, $user->getEmail(), $template, $params,$subject);
        $this->notifier->notify($notification);
    }

    public function onNewAccompte(AccompteEvent $event){
        $accompte = $event->getAccompte();
        $user = $accompte->getChantier()->getUser();
        $subject = 'Création d\'une nouvelle accompte pour le chantier '.$accompte->getChantier()->getName();
        $template = 'Emails/new_accompte_email.html.twig';
        $params = ['user' => $user, 'accompte' =>$accompte , 'chantier' => $accompte->getChantier()];

        $notification = new NotificationObject(NotificationManager::EMAIL_ONLY, $user->getEmail(), $template, $params,$subject);
        $this->notifier->notify($notification);
    }
}