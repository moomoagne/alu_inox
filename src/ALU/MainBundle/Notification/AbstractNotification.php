<?php
/**
 * Created by PhpStorm.
 * User: agne
 * Date: 10/17/18
 * Time: 3:54 PM
 */

namespace ALU\MainBundle\Notification;


use ALU\MainBundle\Entity\NotificationObject;
use ALU\MainBundle\Notification\Gateways\EmailGateway;

abstract class AbstractNotification
{

    protected $emailGateway;

    /**
     * AbstractNotification constructor.
     * @param EmailGateway|null $emailGateway
     */


    public function __construct (EmailGateway $emailGateway = null)
    {
        $this->emailGateway = $emailGateway;
    }

    /**
     * @param NotificationObject $notificatiown
     * @return mixed
     */
    public abstract function notify(NotificationObject $notification);

    public function sendEmail($to, $message, $subject,$params)
    {
        $this->emailGateway->sendEmail($to,$message,$subject,$params);
    }
}