<?php
/**
 * Created by PhpStorm.
 * User: agne
 * Date: 10/17/18
 * Time: 3:37 PM
 */

namespace ALU\MainBundle\Notification\Gateways;


interface EmailGateway
{
    public function sendEmail($to, $message, $subject, $params);
}