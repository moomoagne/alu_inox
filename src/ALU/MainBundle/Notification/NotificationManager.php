<?php
/**
 * Created by PhpStorm.
 * User: agne
 * Date: 10/17/18
 * Time: 4:04 PM
 */

namespace ALU\MainBundle\Notification;


use ALU\MainBundle\Entity\NotificationObject;

class NotificationManager extends AbstractNotification
{
    const EMAIL_ONLY = 1;

    public function notify(NotificationObject $notification)
    {
        // TODO: Implement notify() method.
        $notification->getType();
        $this->sendEmail(
            $notification->getToEmail(),
            $notification->getMailBody(),
            $notification->getSubject(),
            $notification->getParams()
        );
    }
}