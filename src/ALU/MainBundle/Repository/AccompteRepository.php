<?php

namespace ALU\MainBundle\Repository;

/**
 * AccompteRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class AccompteRepository extends \Doctrine\ORM\EntityRepository
{
    public function getAccompteForChantier($id)
    {
        $qb = $this->createQueryBuilder('accompte')
            ->select('accompte')
            ->addSelect('chantier')
            ->leftJoin('accompte.chantier', 'chantier')
            //->from('ALUMainBundle:Accompte', 'accompte')
            //->leftJoin('accompte.chantier','a_chantier')
            ->where('chantier = :chantier_id')
            ->setParameter('chantier_id', $id)
        ;
        return $qb->getQuery()->getResult();
    }

    public function getTotalAccompteForChantier($id)
    {
        $qb = $this->createQueryBuilder('accompte')
            ->addSelect('accompte.montant')
            ->addSelect('chantier')
            ->leftJoin('accompte.chantier', 'chantier')
            ->where('chantier = :chantier_id')
            ->select('SUM(accompte.montant) as accompteTotal')
            ->setParameter('chantier_id',$id)
        ;
        return $qb->getQuery()->getResult();
    }


}
